# **Chef** - by *BUK NANO GRENLAND*

### Features

  - Coming soon...

### Tech

* [npm] - Package manager
* [Ionic 2] - Framework for apps based on Angular 2
* [Google Chrome] - Debugger (ctrl + shift + i)
* [Atom](http://atom.io) - Code editor
* [Sourcetree](https://www.sourcetreeapp.com/) - Source code git GUI
* [Gitlab] - Repository origin
* [Typescript] - Main programming language
* [Html] - Layout
* [Sass] - Stylesheet programming language

### Installation

1. Download and install [NodeJS](https://nodejs.org/en/)
2. Download and install sourcetree
3. Clone this repository with sourcetree to wanted folder
4. Download and install Atom
5. Add package to atom, search for 'terminal'
7. Open terminal in atom within project directory and run

    ```sh
    $ cd 'your/chef/base/directory'
    $ npm install -g ionic cordova
    $ npm install
    $ ionic serve
    ```

Chef should now run, and you can start developing.