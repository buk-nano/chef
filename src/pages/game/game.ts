import { Component, Renderer, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';

// Export command: cordova build --release android --buildConfig
/**
 Windows: START»Command
 $ keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -validity 10000

 Windows: Mac: Terminal
 $ keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -validity 10000
 */

declare var $ : any;
@Component({
  selector: 'page-game',
  templateUrl: 'game.html'
})
export class GamePage {

  constructor(public navCtrl: NavController, renderer: Renderer, Ref: ElementRef) {

    renderer.listenGlobal('document',"keydown", (e) => {

console.log($('#chef').offset().left);
      console.log(e);
      if (e.keyCode==37){
        $('#chef').css('left', ($('#chef').offset().left-10)+'px');
      }
      if (e.keyCode==39){
        $('#chef').css('left', ($('#chef').offset().left+10)+'px');
      }

    });

      renderer.listenGlobal('document',"touchmove", (e) => {

          let w = $(document).width();
          let x = e.touches[0].screenX;
          console.log(e);
          console.log(w);

          if (x<(Math.round(w/2))){
              $('#chef').css('left', ($('#chef').offset().left-10)+'px');
          }
          if (x>(Math.round(w/2))){
              $('#chef').css('left', ($('#chef').offset().left+10)+'px');
          }

      });

  }

}
